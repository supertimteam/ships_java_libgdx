package com.michal5111.ships.enums;

import com.michal5111.ships.utility.Point;

public enum Direction {
    UP(new Point(-1, 0)), DOWN(new Point(1, 0)), LEFT(new Point(0, -1)), RIGHT(new Point(0, 1));

    private final Point point;

    Direction(Point point) {
        this.point = point;
    }

    public Point getPoint() {
        return point;
    }
}
