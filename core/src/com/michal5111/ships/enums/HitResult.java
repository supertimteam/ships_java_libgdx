package com.michal5111.ships.enums;

public enum HitResult {
    MISS, HIT, HIT_AND_SUNK
}
