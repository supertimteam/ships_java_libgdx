package com.michal5111.ships.players;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Disposable;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.michal5111.ships.enums.Direction;
import com.michal5111.ships.events.ClickedFieldEventListener;
import com.michal5111.ships.events.PlayerSelectedFieldEventListener;
import com.michal5111.ships.events.SelectFieldEventListener;
import com.michal5111.ships.screens.PlayerWinScreen;
import com.michal5111.ships.ships.ShipCoordinates;
import com.michal5111.ships.shipsGame.ShipsGame;
import com.michal5111.ships.utility.Point;

import java.io.IOException;

/**
 * Created by Michal on 27.11.2017.
 */

public class PlayerMultiplayerHost extends PlayerHuman implements Disposable {

    private final Server server;
    private Boolean select;

    public PlayerMultiplayerHost() {
        final ShipsGame shipsGame = ShipsGame.getInstance();
        select = false;
        server = new Server();
        server.start();
        Kryo kryo = server.getKryo();
        kryo.register(Point.class);
        kryo.register(ShipCoordinates[][].class);
        kryo.register(ShipCoordinates[].class);
        kryo.register(ShipCoordinates.class);
        kryo.register(Direction.class);
        try {
            server.bind(54555, 54777);
        } catch (IOException e) {
            Gdx.app.log("Server", e.getMessage(), e);
        }
        server.addListener(new Listener() {
            @Override
            public void connected(Connection connection) {
                ShipCoordinates[][] playersShipCoordinates = new ShipCoordinates[][]{
                        shipsGame.getPlayers()[0].getShipCoordinates(),
                        shipsGame.getPlayers()[1].getShipCoordinates()
                };
                server.sendToAllTCP(playersShipCoordinates);
            }

            @Override
            public void received(Connection connection, Object o) {
                if (o instanceof Point) {
                    shipsGame.getPlayers()[1]
                            .getStage()
                            .getRoot()
                            .fire(new PlayerSelectedFieldEventListener.PlayerSelectedFieldEvent(((Point) o)));
                }
            }

            @Override
            public void disconnected(Connection connection) {
                shipsGame.setScreen(new PlayerWinScreen(shipsGame.getBundle().get("playerDisconnected")));
            }
        });
    }

    @Override
    protected void setupListeners() {
        addListener(new SelectFieldEventListener() {
            @Override
            public void select(Stage stage) {
                select = true;
            }
        });
        addListener(new ClickedFieldEventListener() {
            @Override
            public void clicked(Event event, Stage stage) {
                if (select) {
                    Point p = ((ClickedFieldEvent) event).getClickedPoint();
                    if (!getNotMyBoard().getMastAt(p.getX(), p.getY()).isHit()) {
                        stage.getRoot().fire(new PlayerSelectedFieldEventListener.PlayerSelectedFieldEvent(p));
                        server.sendToAllTCP(p);
                        select = false;
                    }
                }
            }
        });
    }

    @Override
    public void dispose() {
        server.close();
        server.stop();
    }

    public Server getServer() {
        return server;
    }
}
