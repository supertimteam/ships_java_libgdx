package com.michal5111.ships.players;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.michal5111.ships.enums.Direction;
import com.michal5111.ships.enums.HitResult;
import com.michal5111.ships.events.ClickedFieldEventListener;
import com.michal5111.ships.events.PlayerSelectedFieldEventListener;
import com.michal5111.ships.events.SelectFieldEventListener;
import com.michal5111.ships.utility.Constants;
import com.michal5111.ships.utility.Point;

import org.jetbrains.annotations.NotNull;

import java.util.Random;

public class PlayerHuman extends Player {

    private final Random random;
    private Boolean select;

    public PlayerHuman() {
        BitmapFont font = new BitmapFont();
        font.getData().setScale(Gdx.graphics.getDensity());
        random = new Random();
        select = false;
        setupListeners();
    }

    void setupListeners() {
        addListener(new SelectFieldEventListener() {
            @Override
            public void select(Stage stage) {
                select = true;
            }
        });
        addListener(new ClickedFieldEventListener() {
            @Override
            public void clicked(Event event, Stage stage) {
                if (select) {
                    Point p = ((ClickedFieldEvent) event).getClickedPoint();
                    if (!getNotMyBoard().getMastAt(p.getX(), p.getY()).isHit()) {
                        stage.getRoot().fire(new PlayerSelectedFieldEventListener.PlayerSelectedFieldEvent(p));
                        select = false;
                    }
                }
            }
        });
    }

    @Override
    protected void onHit(HitResult result, Point p) {
        //Nothing for now
    }

    @NotNull
    @Override
    public final Point getXY() {
        int x;
        int y;
        do {
            x = random.nextInt(Constants.BOARD_SIZE);
            y = random.nextInt(Constants.BOARD_SIZE);
        } while (getNotMyBoard().getMastAt(x, y).isHit());
        return new Point(x, y);
    }

    @Override
    public final Direction getDirection() {
        return Direction.values()[random.nextInt(4)];
    }

    @Override
    public void setShips() {
        for (int i = 0; i < ships.length; i++) {
            while (true) {
                if (setShip(i)) {
                    break;
                }
            }
        }
    }
}
