package com.michal5111.ships.players;

import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Disposable;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.michal5111.ships.enums.Direction;
import com.michal5111.ships.events.ClickedFieldEventListener;
import com.michal5111.ships.events.PlayerSelectedFieldEventListener;
import com.michal5111.ships.events.SelectFieldEventListener;
import com.michal5111.ships.screens.PlayerWinScreen;
import com.michal5111.ships.ships.ShipCoordinates;
import com.michal5111.ships.shipsGame.ShipsGame;
import com.michal5111.ships.utility.Point;

/**
 * Created by Michal on 27.11.2017.
 */

public class PlayerMultiplayerClient extends PlayerHuman implements Disposable {

    private final Client client;
    private Boolean select;

    public PlayerMultiplayerClient() {
        final ShipsGame shipsGame = ShipsGame.getInstance();
        select = false;
        client = new Client();
        client.start();
        Kryo kryo = client.getKryo();
        kryo.register(Point.class);
        kryo.register(ShipCoordinates[][].class);
        kryo.register(ShipCoordinates[].class);
        kryo.register(ShipCoordinates.class);
        kryo.register(Direction.class);
        client.addListener(new Listener() {
            @Override
            public void disconnected(Connection connection) {
                shipsGame.setScreen(new PlayerWinScreen(shipsGame.getBundle().get("playerDisconnected")));
            }

            @Override
            public void received(Connection connection, Object o) {
                if (o instanceof Point) {
                    shipsGame.getPlayers()[1].getStage().getRoot().fire(new PlayerSelectedFieldEventListener.PlayerSelectedFieldEvent(((Point) o)));
                }
                if (o instanceof ShipCoordinates[][]) {
                    for (int i = 0; i < ((ShipCoordinates[][]) o).length; i++) {
                        for (int j = 0; j < ((ShipCoordinates[][]) o)[i].length; j++) {
                            shipsGame.getPlayers()[i].getShips()[j].set(((ShipCoordinates[][]) o)[i][j].getPosition(), ((ShipCoordinates[][]) o)[i][j].getDirection());
                        }
                    }
                }

            }
        });
    }

    @Override
    protected void setupListeners() {
        addListener(new SelectFieldEventListener() {
            @Override
            public void select(Stage stage) {
                select = true;
            }
        });
        addListener(new ClickedFieldEventListener() {
            @Override
            public void clicked(Event event, Stage stage) {
                if (select) {
                    Point p = ((ClickedFieldEvent) event).getClickedPoint();
                    if (!getNotMyBoard().getMastAt(p.getX(), p.getY()).isHit()) {
                        stage.getRoot().fire(new PlayerSelectedFieldEventListener.PlayerSelectedFieldEvent(p));
                        client.sendTCP(p);
                        select = false;
                    }
                }
            }
        });
    }

    @Override
    public void dispose() {
        client.close();
        client.stop();
    }

    public Client getClient() {
        return client;
    }
}
