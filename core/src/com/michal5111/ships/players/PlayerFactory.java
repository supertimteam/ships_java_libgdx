package com.michal5111.ships.players;

import com.badlogic.gdx.Gdx;
import com.michal5111.ships.shipsGame.ShipsGame;

/**
 * Created by michal on 15.02.2018.
 */

public class PlayerFactory {
    public static <T1 extends Player,T2 extends Player> Player[] create(Class<T1> t1Class, Class<T2> t2Class) {
        Player[] players = new Player[2];
        try {
            players[0] = t1Class.newInstance();
            players[1] = t2Class.newInstance();
            players[0].setNotMyBoard(players[1].getMyBoard());
            players[1].setNotMyBoard(players[0].getMyBoard());
            players[0].setShips();
            players[1].setShips();
        } catch (InstantiationException e) {
            Gdx.app.log("ERROR",e.getMessage());
        } catch (IllegalAccessException e) {
            Gdx.app.log("ERROR",e.getMessage());
        }
        return players;
    }
}
