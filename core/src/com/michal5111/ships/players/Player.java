package com.michal5111.ships.players;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.michal5111.ships.board.Board;
import com.michal5111.ships.board.Mast;
import com.michal5111.ships.enums.Direction;
import com.michal5111.ships.enums.HitResult;
import com.michal5111.ships.ships.Ship;
import com.michal5111.ships.ships.Ship1Mast;
import com.michal5111.ships.ships.Ship2Mast;
import com.michal5111.ships.ships.Ship3Mast;
import com.michal5111.ships.ships.Ship4Mast;
import com.michal5111.ships.ships.ShipCoordinates;
import com.michal5111.ships.utility.Constants;
import com.michal5111.ships.utility.Point;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public abstract class Player extends Actor {

    private static int playerID = 0;

    final Ship[] ships;
    private final int id;
    private final ShipCoordinates[] shipCoordinates;
    private Board notMyBoard;
    private Board myBoard;

    protected Player() {
        this.id = playerID++;
        myBoard = new Board(Constants.BOARD_SIZE,Constants.BOARD_SIZE);
        ships = new Ship[]{
                new Ship4Mast(myBoard),
                new Ship3Mast(myBoard),
                new Ship3Mast(myBoard),
                new Ship2Mast(myBoard),
                new Ship2Mast(myBoard),
                new Ship2Mast(myBoard),
                new Ship1Mast(myBoard),
                new Ship1Mast(myBoard),
                new Ship1Mast(myBoard)
        };
        shipCoordinates = new ShipCoordinates[9];
    }

    public final HitResult hit(Point p) {
        Mast mast = notMyBoard.getMastAt(p);
        if (mast.isHit()) {
            throw new IllegalArgumentException("Point is already hit");
        }
        mast.hit();
        if (mast.isOwned()) {
            if (mast.getOwner().isSunk()) {
                onHit(HitResult.HIT_AND_SUNK, p);
                return HitResult.HIT_AND_SUNK;
            }
            onHit(HitResult.HIT, p);
            return HitResult.HIT;
        }
        onHit(HitResult.MISS, p);
        return HitResult.MISS;
    }

    protected abstract void onHit(HitResult result, Point p);

    protected abstract Point getXY();

    protected abstract Direction getDirection();

    @Contract(pure = true)
    public final int geId() {
        return id;
    }

    public final boolean didLose() {
        for (Ship ship : ships) {
            if (!ship.isSunk()) {
                return false;
            }
        }
        return true;
    }

    @NotNull
    final Boolean setShip(int number) {
        Direction direction = null;
        Point p = null;
        boolean result = false;
        while (!result) {
            p = new Point(getXY());
            if (ships[number].getSize() == 1) {
                direction = Direction.UP;
            } else {
                direction = getDirection();
            }
            result = ships[number].set(p, direction);

            if (!result) {
                return false;
            }
        }
        shipCoordinates[number] = new ShipCoordinates(p, direction);
        return true;
    }

    public abstract void setShips();

    @Contract(pure = true)
    Ship[] getShips() {
        return ships;
    }

    public int getUnsunkenShipsCount() {
        int counter = 0;
        for (Ship ship : getShips()) {
            if (!ship.isSunk()) {
                counter++;
            }
        }
        return counter;
    }

    public Board getMyBoard() {
        return myBoard;
    }

    public Board getNotMyBoard() {
        return notMyBoard;
    }

    public void setNotMyBoard(Board notMyBoard) {
        this.notMyBoard = notMyBoard;
    }

    ShipCoordinates[] getShipCoordinates() {
        return shipCoordinates;
    }

    public void showShips(boolean show) {
        for (Ship ship : getShips()) {
            ship.showShip(show);
        }
    }
}
