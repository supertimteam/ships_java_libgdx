package com.michal5111.ships.players;

import com.michal5111.ships.enums.Direction;
import com.michal5111.ships.enums.HitResult;
import com.michal5111.ships.utility.Constants;
import com.michal5111.ships.utility.Point;

import org.jetbrains.annotations.NotNull;

import java.util.Random;

/**
 * Created by Michal on 29.11.2017.
 */

public class PlayerMultiplayerDummy extends Player {
    private final Random random;

    public PlayerMultiplayerDummy() {
        random = new Random();
    }

    @Override
    protected void onHit(HitResult result, Point p) {
        //Nothing for now
    }

    @NotNull
    @Override
    public final Point getXY() {
        int x;
        int y;
        do {
            x = random.nextInt(Constants.BOARD_SIZE);
            y = random.nextInt(Constants.BOARD_SIZE);
        } while (getNotMyBoard().getMastAt(x, y).isHit());
        return new Point(x, y);
    }

    @Override
    public final Direction getDirection() {
        return Direction.values()[random.nextInt(4)];
    }

    @Override
    public void setShips() {
        for (int i = 0; i < ships.length; i++) {
            while (true) {
                if (setShip(i)) {
                    break;
                }
            }
        }
    }
}
