package com.michal5111.ships.utility;

import org.jetbrains.annotations.Contract;

public class Point {

    private final int x;
    private final int y;

    private Point() {
        this.x = 0;
        this.y = 0;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point p) {
        x = p.getX();
        y = p.getY();
    }

    @Contract(pure = true)
    public static boolean checkPoint(int x, int y) {
        return !(x < 0 || x > Constants.BOARD_SIZE - 1 || y < 0 || y > Constants.BOARD_SIZE - 1);
    }

    public static boolean checkPoint(Point p) {
        return !(p.getX() < 0 || p.getX() > Constants.BOARD_SIZE - 1 || p.getY() < 0 || p.getY() > Constants.BOARD_SIZE - 1);
    }

    @Contract(pure = true)
    public final int getX() {
        return x;
    }

    @Contract(pure = true)
    public final int getY() {
        return y;
    }

    public String toString() {
        return String.valueOf((char) (x + 65)) + (y + 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
