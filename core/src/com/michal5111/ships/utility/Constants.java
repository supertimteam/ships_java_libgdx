package com.michal5111.ships.utility;

public final class Constants {
    public static final int BOARD_SIZE = 10;

    private Constants() {
    }
}
