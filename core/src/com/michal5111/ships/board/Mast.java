package com.michal5111.ships.board;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.michal5111.ships.events.ClickedFieldEventListener;
import com.michal5111.ships.ships.Ship;
import com.michal5111.ships.shipsGame.ShipsGame;
import com.michal5111.ships.utility.Point;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public class Mast extends Image {

    private final Point pos;
    private final ShipsGame shipsGame;
    private final Texture emptyField;
    private final Texture emptyHit;
    private final Texture shipFieldHit;
    private final Texture shipFieldSunk;
    private final Sound explosion;
    private final Sound splash;
    private final Texture shipField;
    private boolean hit;
    private Ship ship;

    Mast(final Point pos) {
        this.pos = pos;
        this.shipsGame = ShipsGame.getInstance();
        this.hit = false;
        this.ship = null;
        emptyField = shipsGame.getAssetManager().get("emptyField.png");
        emptyHit = shipsGame.getAssetManager().get("emptyFieldHit.png");
        shipFieldHit = shipsGame.getAssetManager().get("shipFieldHit.png");
        shipFieldSunk = shipsGame.getAssetManager().get("shipFieldSunk.png");
        shipField = shipsGame.getAssetManager().get("shipField.png");
        setDrawable(updateTexture(false));
        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                for (Actor actor : event.getStage().getActors()) {
                    actor.fire(new ClickedFieldEventListener.ClickedFieldEvent(getPos()));
                }
            }
        });
        explosion = shipsGame.getAssetManager().get("explosion.ogg");
        splash = shipsGame.getAssetManager().get("splash.ogg");

    }

    public final void hit() {
        hit = true;
        setDrawable(updateTexture(false));
        if (isOwned()) {
            explosion.play(shipsGame.getSoundManager().getEffectsVolume());
        } else {
            splash.play(shipsGame.getSoundManager().getEffectsVolume());
        }
    }

    @Contract(pure = true)
    public final boolean isHit() {
        return hit;
    }

    @Contract(pure = true)
    public final boolean isOwned() {
        return ship != null;
    }

    @Contract(pure = true)
    public final Ship getOwner() {
        return ship;
    }

    public final void setOwner(Ship ship) {
        this.ship = ship;
        setDrawable(updateTexture(false));
    }

    @NotNull
    public SpriteDrawable updateTexture(boolean show) {
        if (isHit()) {
            if (isOwned()) return new SpriteDrawable(new Sprite(shipFieldHit));
            return new SpriteDrawable(new Sprite(emptyHit));
        }
        if (show && isOwned()) {
            return new SpriteDrawable(new Sprite(shipField));
        }
        return new SpriteDrawable(new Sprite(emptyField));
    }

    @Contract(pure = true)
    private Point getPos() {
        return pos;
    }

    public void setSunk() {
        setDrawable(new SpriteDrawable(new Sprite(shipFieldSunk)));
    }
}
