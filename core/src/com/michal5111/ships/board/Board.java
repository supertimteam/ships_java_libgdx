package com.michal5111.ships.board;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.michal5111.ships.utility.Point;

/**
 * Created by Michal on 20.11.2017.
 */

public class Board {

    private final Mast[][] boardArray;
    private final Table boardTable;

    public Board(int sizeX, int sizeY) {
        BitmapFont smallFont = new BitmapFont();
        Label.LabelStyle labelStyle = new Label.LabelStyle(smallFont, Color.RED);
        boardArray = new Mast[sizeX][sizeY];
        boardTable = new Table();
        boardTable.add(new Label(" ", labelStyle));
        for (int i = 0; i < boardArray.length; i++) {
            Label label2 = new Label(Integer.toString(i + 1) + "", labelStyle);
            label2.setFontScale(Gdx.graphics.getDensity());
            boardTable.add(label2);
        }
        boardTable.row();
        for (int i = 0; i < boardArray.length; i++) {
            Label label = new Label(Character.toString((char) (i + 65)) + "", labelStyle);
            label.setFontScale(Gdx.graphics.getDensity());
            boardTable.add(label);
            for (int j = 0; j < boardArray.length; j++) {
                boardArray[i][j] = new Mast(new Point(i, j));
                boardTable.add(boardArray[i][j])
                        .expand()
                        .fill()
                        .uniform();
            }
            boardTable.row();
        }
    }

    public Table getBoardTable() {
        return boardTable;
    }

    public Mast getMastAt(int x, int y) {
        try {
            return boardArray[x][y];
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Point out of boardArray bounds");
        }
    }

    public Mast getMastAt(Point point) {
        try {
            return boardArray[point.getX()][point.getY()];
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Point out of boardArray bounds");
        }
    }
}
