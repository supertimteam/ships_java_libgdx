package com.michal5111.ships.ships;

import com.michal5111.ships.board.Board;

public class Ship2Mast extends Ship {

    public Ship2Mast(Board board) {
        //noinspection SpellCheckingInspection
        super(board, 2, "dwumasztowy");
    }
}
