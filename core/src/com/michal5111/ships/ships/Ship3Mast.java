package com.michal5111.ships.ships;

import com.michal5111.ships.board.Board;

public class Ship3Mast extends Ship {

    public Ship3Mast(Board board) {
        //noinspection SpellCheckingInspection
        super(board, 3, "trzymasztowy");
    }
}
