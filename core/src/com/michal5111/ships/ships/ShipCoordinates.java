package com.michal5111.ships.ships;

import com.michal5111.ships.enums.Direction;
import com.michal5111.ships.utility.Point;

/**
 * Created by Michal on 30.11.2017.
 */

public class ShipCoordinates {
    private Point position;
    private Direction direction;

    private ShipCoordinates() {
    }

    public ShipCoordinates(Point position, Direction direction) {
        this.position = position;
        this.direction = direction;
    }

    public Point getPosition() {
        return position;
    }

    public Direction getDirection() {
        return direction;
    }
}
