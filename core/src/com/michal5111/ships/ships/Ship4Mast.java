package com.michal5111.ships.ships;

import com.michal5111.ships.board.Board;

public class Ship4Mast extends Ship {

    public Ship4Mast(Board board) {
        //noinspection SpellCheckingInspection
        super(board, 4, "czteromasztowy");
    }
}
