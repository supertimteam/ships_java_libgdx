package com.michal5111.ships.ships;

import com.michal5111.ships.board.Board;

public class Ship1Mast extends Ship {

    public Ship1Mast(Board board) {
        //noinspection SpellCheckingInspection
        super(board, 1, "jednomasztowy");
    }
}
