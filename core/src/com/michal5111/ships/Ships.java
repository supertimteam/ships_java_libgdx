package com.michal5111.ships;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.kotcrab.vis.ui.VisUI;
import com.michal5111.ships.shipsGame.ShipsGame;

public class Ships extends ApplicationAdapter {

    private ShipsGame shipsGame;

    @Override
    public void create() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.input.setCatchBackKey(true);
        VisUI.load(VisUI.SkinScale.X2);
        shipsGame = new ShipsGame();
        shipsGame.create();
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        shipsGame.render();
    }

    @Override
    public void dispose() {
        shipsGame.dispose();
        VisUI.dispose();
    }
}
