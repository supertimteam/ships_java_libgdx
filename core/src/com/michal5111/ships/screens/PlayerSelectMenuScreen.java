package com.michal5111.ships.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisSelectBox;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.michal5111.ships.players.PlayerComputer;
import com.michal5111.ships.players.PlayerFactory;
import com.michal5111.ships.players.PlayerHuman;
import com.michal5111.ships.shipsGame.ShipsGame;

/**
 * Created by Michal on 07.11.2017.
 */

class PlayerSelectMenuScreen implements Screen {

    private static final String SELECT_PLAYER = "selectPlayer";
    private final ShipsGame shipsGame;
    private final Stage stage;
    private final Table table;
    private final Label label;
    private int id;
    private VisTextButton playButton;
    private VisSelectBox<String> playerOneSelectBox;
    private VisSelectBox<String> playerTwoSelectBox;

    public PlayerSelectMenuScreen() {
        Image background = new Image(new Texture("backgroundMenu.png"));
        background.setBounds(
                0,
                0,
                Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight());
        this.shipsGame = ShipsGame.getInstance();
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        id = 0;
        label = new VisLabel(shipsGame.getBundle().format(SELECT_PLAYER, (id + 1)));
        label.setFontScale(2);
        //label.setFontScale(Gdx.graphics.getDensity() * 2);
        table = new VisTable();
        playerOneSelectBox = new VisSelectBox<String>();
        playerOneSelectBox.setItems(shipsGame.getBundle().get("human"), shipsGame.getBundle().get("computer"));
        playerTwoSelectBox = new VisSelectBox<String>();
        playerTwoSelectBox.setItems(shipsGame.getBundle().get("computer"), shipsGame.getBundle().get("human"));
        playButton = new VisTextButton(shipsGame.getBundle().get("play"));
        table.setFillParent(true);
        table.add(label).expandY().colspan(2).bottom().row();
        table.add(playerOneSelectBox).expand().fillX().pad(50).height(100);
        table.add(playerTwoSelectBox).expand().fillX().pad(50).height(100).row();
        table.add(playButton).colspan(2).grow().pad(50);
        stage.addActor(background);
        stage.addActor(table);
    }

    @Override
    public void show() {
        playButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Class[] classes = new Class[2];
                if (playerOneSelectBox.getSelected().equals(shipsGame.getBundle().get("computer"))) {
                    classes[0] = PlayerComputer.class;
                } else classes[0] =  PlayerHuman.class;
                if (playerTwoSelectBox.getSelected().equals(shipsGame.getBundle().get("computer"))) {
                    classes[1] = PlayerComputer.class;
                } else classes[1] = PlayerHuman.class;
                shipsGame.setPlayers(PlayerFactory.create(classes[0],classes[1]));
                dispose();
                shipsGame.setScreen(new SwitchPlayerScreen(shipsGame.getBundle().format("playerTurn", (shipsGame.getTurn() + 1))));
            }
        });
    }

    @Override
    public void render(float delta) {
        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        //Nothing for now
    }

    @Override
    public void pause() {
        //Nothing for now
    }

    @Override
    public void resume() {
        //Nothing for now
    }

    @Override
    public void hide() {
        //Nothing for now
    }

    @Override
    public void dispose() {
        table.remove();
        stage.dispose();
        label.remove();
    }
}
