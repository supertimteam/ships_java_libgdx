package com.michal5111.ships.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.TimeUtils;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisWindow;
import com.michal5111.ships.enums.HitResult;
import com.michal5111.ships.events.PlayerSelectedFieldEventListener;
import com.michal5111.ships.events.SelectFieldEventListener;
import com.michal5111.ships.players.Player;
import com.michal5111.ships.players.PlayerHuman;
import com.michal5111.ships.shipsGame.ShipsGame;
import com.michal5111.ships.utility.Point;

/**
 * Created by Michal on 07.11.2017.
 */

public class PlaySelectFieldScreen implements Screen {

    private static final int DELAY = 1000;
    private final Player opponent;
    private final Player player;
    private final ShipsGame shipsGame;
    private Table superTable;
    private Table textTable;
    private Table menuTable;
    private Table playersBoardTable;
    private Label selectedFieldLabel;
    private Label resultLabel;
    private Point field;
    private Stage stage;
    private Button exitButton;
    private Boolean pause;
    private Boolean selected;
    private result playResult;
    private TextButton showPlayersBoard;
    private boolean playerTableIsShown = false;
    private long millisStart;
    PlaySelectFieldScreen(final Player player, final Player opponent) {
        opponent.showShips(false);
        this.shipsGame = ShipsGame.getInstance();
        this.opponent = opponent;
        this.player = player;
        selected = false;
        pause = false;
        setupExitMenu();
        setupTextTable();
        setupStage();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {
        shipsGame.incrementRounds();
        shipsGame.getPlayers()[shipsGame.getTurn()].fire(new SelectFieldEventListener.SelectFieldEvent(stage));
    }

    @Override
    public void render(float delta) {
        stage.draw();
        stage.act();
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)) {
            if (!pause) {
                pause();
            } else {
                resume();
            }
        }
        if (TimeUtils.millis() - millisStart > DELAY && selected && !pause) {
            switch (playResult) {
                case NEXT_ROUND:
                    dispose();
                    shipsGame.setScreen(new PlaySelectFieldScreen(player, opponent));
                    break;
                case SWITCH_ROUND:
                    dispose();
                    shipsGame.setScreen(new SwitchPlayerScreen(
                            shipsGame.getBundle().format(
                                    "playerTurn",
                                    (shipsGame.getTurn() + 1)))
                    );
                    break;
                case WIN:
                    dispose();
                    shipsGame.setScreen(new PlayerWinScreen(shipsGame.getBundle().format("playerWins", (player.geId() + 1))));
                    break;
            }
        }
    }

    @Override
    public void resize(int width, int height) {
        //Nothing for now
    }

    @Override
    public void pause() {
        pause = true;
        stage.addActor(menuTable);
    }

    @Override
    public void resume() {
        pause = false;
        millisStart = TimeUtils.millis();
        menuTable.remove();
    }

    @Override
    public void hide() {
        pause();
    }

    @Override
    public void dispose() {
        resultLabel.remove();
        selectedFieldLabel.remove();
        stage.dispose();
        textTable.remove();
    }

    private result play(Player player, Player opponent) {
        selectedFieldLabel.setText(field.toString());
        HitResult result = player.hit(field);
        if (opponent.didLose()) {
            return PlaySelectFieldScreen.result.WIN;
        } else switch (result) {
            case HIT:
                resultLabel.setText(shipsGame.getBundle().get("hit"));
                return PlaySelectFieldScreen.result.NEXT_ROUND;
            case HIT_AND_SUNK:
                resultLabel.setText(shipsGame.getBundle().get("hitAndSunk"));
                return PlaySelectFieldScreen.result.NEXT_ROUND;
            case MISS:
                resultLabel.setText(shipsGame.getBundle().get("miss"));
                shipsGame.switchTurn();
                return PlaySelectFieldScreen.result.SWITCH_ROUND;
        }
        throw new IllegalStateException();
    }

    private void setupExitMenu() {
        exitButton = new VisTextButton(shipsGame.getBundle().get("exit"));
        exitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pause();
            }
        });
        exitButton.setPosition(
                Gdx.graphics.getWidth() - exitButton.getWidth() - 20,
                Gdx.graphics.getHeight() - exitButton.getHeight() - 20
        );
        exitButton.setScale(Gdx.graphics.getDensity());
        menuTable = new VisWindow("Pauza");
        menuTable.setBounds(
                100,
                100,
                Gdx.graphics.getWidth() - 200f,
                Gdx.graphics.getHeight() - 200f
        );
        TextButton exitMenu = new VisTextButton(shipsGame.getBundle().get("menu"));
        exitMenu.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dispose();
                shipsGame.create();
            }
        });
        TextButton settingsButton = new VisTextButton(shipsGame.getBundle().get("settings"));
        settingsButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                PlaySelectFieldScreen.this.shipsGame.setScreen(
                        new SettingsScreen(shipsGame, PlaySelectFieldScreen.this, stage)
                );
            }
        });
        Button exitTableMenuButton = new VisTextButton("X");
        exitTableMenuButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                resume();
            }
        });
        menuTable.add(exitTableMenuButton).right().top().expand().row();
        menuTable.add(settingsButton).row();
        menuTable.add(exitMenu);
    }

    private void setupTextTable() {
        selectedFieldLabel = new VisLabel("");
        resultLabel = new VisLabel(shipsGame.getBundle().get("selectField"));
        playersBoardTable = new Table();
        playersBoardTable.add(player.getMyBoard().getBoardTable());
        playersBoardTable.setTouchable(Touchable.disabled);
        showPlayersBoard = new VisTextButton(
                shipsGame.getBundle().get("myBoard")
        );
        showPlayersBoard.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (!playerTableIsShown) {
                    player.showShips(true);
                    textTable.add(playersBoardTable);
                    playerTableIsShown = true;
                } else {
                    textTable.removeActor(playersBoardTable);
                    playerTableIsShown = false;
                }
            }
        });
        Label shipsLeftLabel = new VisLabel(
                shipsGame.getBundle()
                        .format("opponentShipsCount", opponent.getUnsunkenShipsCount())
        );
        Label yourShipsLeftLabel = new VisLabel(
                shipsGame.getBundle()
                        .format("playerShipsCount", player.getUnsunkenShipsCount())
        );
        Label roundNumberLabel = new VisLabel(
                shipsGame.getBundle()
                        .format("round", shipsGame.getRounds())
        );
        textTable = new Table();
        if (player.getClass().isAssignableFrom(PlayerHuman.class))
            textTable.add(showPlayersBoard).row();
        textTable.add(roundNumberLabel).row();
        textTable.add(yourShipsLeftLabel).row();
        textTable.add(shipsLeftLabel).row();
        textTable.add(selectedFieldLabel).row();
        textTable.add(resultLabel).row();
    }

    private void setupStage() {
        stage = new Stage();
        superTable = new Table();
        superTable.setFillParent(true);
        stage.addActor(superTable);
        superTable.add(exitButton).right().row();
        superTable.add(textTable).grow().row();
        superTable.add(opponent.getMyBoard().getBoardTable()).grow().uniform();
        stage.addListener(new PlayerSelectedFieldEventListener() {
            @Override
            public void select(PlayerSelectedFieldEvent event) {
                field = event.getSelectedField();
                playResult = play(player, opponent);
                selected = true;
                millisStart = TimeUtils.millis();
            }
        });
        stage.addActor(shipsGame.getPlayers()[0]);
        stage.addActor(shipsGame.getPlayers()[1]);
    }

    enum result {
        NEXT_ROUND, SWITCH_ROUND, WIN
    }
}
