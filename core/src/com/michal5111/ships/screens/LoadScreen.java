package com.michal5111.ships.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.I18NBundle;
import com.michal5111.ships.shipsGame.ShipsGame;

/**
 * Created by Michal on 08.11.2017.
 */

public class LoadScreen implements Screen {
    private final Stage stage;
    private final Table table;
    private final ShipsGame shipsGame;
    private final AssetManager assetManager;

    public LoadScreen() {
        this.shipsGame = ShipsGame.getInstance();
        this.assetManager = shipsGame.getAssetManager();
        table = new Table();
        table.setFillParent(true);
        stage = new Stage();
        stage.addActor(table);
        assetManager.load("backgroundMenu.png", Texture.class);
        assetManager.load("ui-red.atlas", TextureAtlas.class);
        assetManager.load("emptyField.png", Texture.class);
        assetManager.load("emptyFieldHit.png", Texture.class);
        assetManager.load("shipFieldHit.png", Texture.class);
        assetManager.load("shipFieldSunk.png", Texture.class);
        assetManager.load("bGMusic.ogg", Music.class);
        assetManager.load("explosion.ogg", Sound.class);
        assetManager.load("splash.ogg", Sound.class);
        assetManager.load("MyBundle", I18NBundle.class);
        assetManager.load("shipField.png", Texture.class);
        assetManager.load("exp.png",Texture.class);
    }

    @Override
    public void show() {
        //Noting for now.
    }

    @Override
    public void render(float delta) {
        stage.draw();
        if (shipsGame.getAssetManager().update()) {
            shipsGame.afterLoading();
            shipsGame.setScreen(new MenuScreen());
        }
    }

    @Override
    public void resize(int width, int height) {
        //Noting for now.
    }

    @Override
    public void pause() {
        //Noting for now.
    }

    @Override
    public void resume() {
        //Noting for now.
    }

    @Override
    public void hide() {
        //Noting for now.
    }

    @Override
    public void dispose() {
        stage.dispose();
        table.remove();
    }
}
