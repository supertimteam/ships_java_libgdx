package com.michal5111.ships.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.michal5111.ships.shipsGame.ShipsGame;

/**
 * Created by Michal on 07.11.2017.
 */

public class MenuScreen implements Screen {
    private final ShipsGame shipsGame;
    private final Stage stage;
    private final VisTable table;

    public MenuScreen() {
        final ShipsGame shipsGame = ShipsGame.getInstance();
        Image background = new Image(shipsGame.getAssetManager().get("backgroundMenu.png", Texture.class));
        background.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        VisTextButton playButton = new VisTextButton(shipsGame.getBundle().get("play"));
        VisTextButton exitButton = new VisTextButton(shipsGame.getBundle().get("exit"));
        VisTextButton settingsButton = new VisTextButton(shipsGame.getBundle().get("settings"));
        VisTextButton multiplayerButton = new VisTextButton(shipsGame.getBundle().get("multiplayer"));
        this.shipsGame = shipsGame;
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        table = new VisTable();
        table.setFillParent(true);
        table.add(playButton)
                .expand()
                .bottom()
                .center()
                .padRight(Gdx.graphics.getWidth() / 5f)
                .padLeft(Gdx.graphics.getWidth() / 5f)
                .height(Gdx.graphics.getHeight() / 5f).fillX()
                .row();
        table.add(multiplayerButton)
                .expand()
                .bottom()
                .center()
                .padRight(Gdx.graphics.getWidth() / 5f)
                .padLeft(Gdx.graphics.getWidth() / 5f)
                .height(Gdx.graphics.getHeight() / 5f)
                .fillX()
                .row();
        table.add(settingsButton)
                .expand()
                .bottom()
                .center()
                .padRight(Gdx.graphics.getWidth() / 5f)
                .padLeft(Gdx.graphics.getWidth() / 5f)
                .height(Gdx.graphics.getHeight() / 5f)
                .fillX()
                .row();
        table.add(exitButton)
                .expand()
                .bottom()
                .center()
                .padRight(Gdx.graphics.getWidth() / 5f)
                .padLeft(Gdx.graphics.getWidth() / 5f)
                .height(Gdx.graphics.getHeight() / 5f)
                .fillX();
        stage.addActor(background);
        stage.addActor(table);
        playButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dispose();
                shipsGame.setScreen(new PlayerSelectMenuScreen());
            }
        });
        settingsButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                shipsGame.setScreen(new SettingsScreen(shipsGame, MenuScreen.this, stage));
            }
        });
        exitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });
        multiplayerButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dispose();
                shipsGame.setScreen(new com.michal5111.ships.screens.multiplayer.MultiplayerMenuScreen(shipsGame));
            }
        });
    }


    @Override
    public void show() {
        //Noting for now.
    }

    @Override
    public void render(float delta) {
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        //Noting for now.
    }

    @Override
    public void pause() {
        //Noting for now.
    }

    @Override
    public void resume() {
        //Noting for now.
    }

    @Override
    public void hide() {
        //Noting for now.
    }

    @Override
    public void dispose() {
        stage.dispose();
        table.remove();
    }
}
