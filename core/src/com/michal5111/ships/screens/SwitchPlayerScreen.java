package com.michal5111.ships.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.TimeUtils;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.michal5111.ships.shipsGame.ShipsGame;

/**
 * Created by Michal on 07.11.2017.
 */

public class SwitchPlayerScreen implements Screen {

    private static final int DELAY = 1000;
    private final ShipsGame shipsGame;
    private final Stage stage;
    private final Table table;
    private final Boolean change;
    private long millisStart;


    public SwitchPlayerScreen(String text) {
        this.shipsGame = ShipsGame.getInstance();
        change = false;
        stage = new Stage();
        table = new Table();
        table.setFillParent(true);
        TextButton textButton = new VisTextButton(text);
        table.add(textButton)
                .expand()
                .bottom()
                .center()
                .padRight(Gdx.graphics.getWidth() / 5f)
                .padLeft(Gdx.graphics.getWidth() / 5f)
                .height(Gdx.graphics.getHeight() / 5f)
                .fillX();
        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {
        millisStart = TimeUtils.millis();
    }

    @Override
    public void render(float delta) {
        stage.draw();
        if (TimeUtils.millis() - millisStart >= DELAY || change) {
            if (shipsGame.getTurn() == 0) {
                dispose();
                shipsGame.setScreen(new PlaySelectFieldScreen(
                        shipsGame.getPlayers()[0],
                        shipsGame.getPlayers()[1])
                );
            } else {
                dispose();
                shipsGame.setScreen(new PlaySelectFieldScreen(
                        shipsGame.getPlayers()[1],
                        shipsGame.getPlayers()[0])
                );
            }
        }
    }

    @Override
    public void resize(int width, int height) {
        //Noting for now.
    }

    @Override
    public void pause() {
        //Noting for now.
    }

    @Override
    public void resume() {
        //Noting for now.
    }

    @Override
    public void hide() {
        //Noting for now.
    }

    @Override
    public void dispose() {
        stage.dispose();
        table.remove();
    }
}
