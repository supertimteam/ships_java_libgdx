package com.michal5111.ships.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.michal5111.ships.shipsGame.ShipsGame;

/**
 * Created by Michal on 08.11.2017.
 */

public class PlayerWinScreen implements Screen {
    private final Stage stage;
    private final Table table;
    private final TextButton textButton;
    private final ShipsGame shipsGame;

    public PlayerWinScreen(String message) {
        this.shipsGame = ShipsGame.getInstance();
        textButton = new VisTextButton(message);
        table = new Table();
        table.setFillParent(true);
        table.add(textButton)
                .expand()
                .bottom()
                .center()
                .padRight(Gdx.graphics.getWidth() / 5f)
                .padLeft(Gdx.graphics.getWidth() / 5f)
                .height(Gdx.graphics.getHeight() / 5f)
                .fillX();
        stage = new Stage();
        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);
        shipsGame.getSoundManager().getBGMusic().stop();
    }

    @Override
    public void show() {
        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dispose();
                shipsGame.create();
            }
        });
    }

    @Override
    public void render(float delta) {
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        //Noting for now
    }

    @Override
    public void pause() {
        //Noting for now
    }

    @Override
    public void resume() {
        //Noting for now
    }

    @Override
    public void hide() {
        //Noting for now
    }

    @Override
    public void dispose() {
        textButton.remove();
        stage.dispose();
        table.remove();
    }
}
