package com.michal5111.ships.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisSlider;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.michal5111.ships.shipsGame.ShipsGame;

/**
 * Created by Michal on 07.11.2017.
 */

class SettingsScreen implements Screen {
    private final ShipsGame shipsGame;
    private final Stage stage;
    private final Table table;
    private final VisCheckBox checkBox;

    public SettingsScreen(ShipsGame game, final Screen returnScreen, final Stage returnStage) {
        Image background = new Image(new Texture("backgroundMenu.png"));
        background.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        this.shipsGame = game;
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);
        table = new Table();
        table.setFillParent(true);
        stage.addActor(background);
        stage.addActor(table);
        checkBox = new VisCheckBox(shipsGame.getBundle().get("music"));
        checkBox.setChecked(shipsGame
                .getSoundManager()
                .getBGMusic()
                .isPlaying());
        TextButton textButton = new VisTextButton(shipsGame.getBundle().get("back"));
        final Slider sliderMusic = new VisSlider(0, 1, 0.01f, false);
        sliderMusic.setValue(shipsGame.getSoundManager().getBGMusic().getVolume());
        final Slider sliderEffects = new VisSlider(0, 1, 0.01f, false);
        sliderEffects.setValue(shipsGame.getSoundManager().getEffectsVolume());
        Label musicLabel = new VisLabel(shipsGame.getBundle().get("musicVolume"));
        Label effectsLabel = new VisLabel(shipsGame.getBundle().get("effectsVolume"));
        table.add(checkBox).expandX().fill().row();
        table.add(musicLabel).row();
        table.add(sliderMusic)
                .expandX()
                .fill()
                .height(100)
                .row();
        table.add(effectsLabel).row();
        table.add(sliderEffects)
                .expandX()
                .fill()
                .height(100)
                .row();
        table.add(textButton);
        checkBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (!checkBox.isChecked()) {
                    shipsGame.getSoundManager().getBGMusic().stop();
                    shipsGame.getPreferences().putBoolean("musicOn", false);
                } else {
                    shipsGame.getSoundManager().getBGMusic().play();
                    shipsGame.getPreferences().putBoolean("musicOn", true);
                }
            }
        });
        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                shipsGame.getPreferences().flush();
                dispose();
                Gdx.input.setInputProcessor(returnStage);
                SettingsScreen.this.shipsGame.setScreen(returnScreen);
            }
        });
        sliderMusic.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                shipsGame.getSoundManager().getBGMusic().setVolume(sliderMusic.getValue());
                shipsGame.getPreferences().putFloat("musicVolume", sliderMusic.getValue());
            }
        });
        sliderEffects.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                shipsGame.getSoundManager().setEffectsVolume(sliderEffects.getValue());
                if (!sliderEffects.isDragging())
                    shipsGame.getAssetManager().get("explosion.ogg", Sound.class).play(sliderEffects.getValue());
                shipsGame.getPreferences().putFloat("effectsVolume", sliderEffects.getValue());
            }
        });
    }


    @Override
    public void show() {
        //Noting for now.
    }

    @Override
    public void render(float delta) {
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        //Noting for now.
    }

    @Override
    public void pause() {
        //Noting for now.
    }

    @Override
    public void resume() {
        //Noting for now.
    }

    @Override
    public void hide() {
        //Noting for now.
    }

    @Override
    public void dispose() {
        stage.dispose();
        table.remove();
    }
}
