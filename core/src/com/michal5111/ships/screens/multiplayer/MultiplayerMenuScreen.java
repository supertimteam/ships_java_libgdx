package com.michal5111.ships.screens.multiplayer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.michal5111.ships.players.PlayerFactory;
import com.michal5111.ships.players.PlayerMultiplayerClient;
import com.michal5111.ships.players.PlayerMultiplayerDummy;
import com.michal5111.ships.players.PlayerMultiplayerHost;
import com.michal5111.ships.screens.MenuScreen;
import com.michal5111.ships.shipsGame.ShipsGame;

/**
 * Created by Michal on 07.11.2017.
 */

public class MultiplayerMenuScreen implements Screen {
    private final Stage stage;
    private final Table table;
    private final ShipsGame shipsGame;

    public MultiplayerMenuScreen(final ShipsGame shipsGame) {
        this.shipsGame = shipsGame;
        Image background = new Image(shipsGame.getAssetManager().get("backgroundMenu.png", Texture.class));
        background.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        TextButton hostButton = new VisTextButton(shipsGame.getBundle().get("host"));
        TextButton exitButton = new VisTextButton(shipsGame.getBundle().get("exit"));
        TextButton clientButton = new VisTextButton(shipsGame.getBundle().get("client"));
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);
        table = new Table();
        table.setFillParent(true);
        table.add(hostButton)
                .expand()
                .bottom()
                .center()
                .padRight(Gdx.graphics.getWidth() / 5f)
                .padLeft(Gdx.graphics.getWidth() / 5f)
                .height(Gdx.graphics.getHeight() / 5f)
                .fillX()
                .row();
        table.add(clientButton)
                .expand()
                .bottom()
                .center()
                .padRight(Gdx.graphics.getWidth() / 5f)
                .padLeft(Gdx.graphics.getWidth() / 5f)
                .height(Gdx.graphics.getHeight() / 5f)
                .fillX()
                .row();
        table.add(exitButton)
                .expand()
                .bottom()
                .center()
                .padRight(Gdx.graphics.getWidth() / 5f)
                .padLeft(Gdx.graphics.getWidth() / 5f)
                .height(Gdx.graphics.getHeight() / 5f)
                .fillX();
        stage.addActor(background);
        stage.addActor(table);
        hostButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                shipsGame.setPlayers(PlayerFactory.create(PlayerMultiplayerHost.class,PlayerMultiplayerDummy.class));
                dispose();
                shipsGame.setScreen(new com.michal5111.ships.screens.multiplayer.HostWaitForConnectionScreen(((PlayerMultiplayerHost) shipsGame.getPlayers()[0]).getServer()));
            }
        });
        clientButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                shipsGame.setPlayers(PlayerFactory.create(PlayerMultiplayerDummy.class,PlayerMultiplayerClient.class));
                dispose();
                shipsGame.setScreen(new com.michal5111.ships.screens.multiplayer.ClientConnectionScreen(((PlayerMultiplayerClient) shipsGame.getPlayers()[1]).getClient()));
            }
        });
        exitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dispose();
                shipsGame.setScreen(new MenuScreen());
            }
        });
    }


    @Override
    public void show() {
        //Noting for now.
    }

    @Override
    public void render(float delta) {
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        //Noting for now.
    }

    @Override
    public void pause() {
        //Noting for now.
    }

    @Override
    public void resume() {
        //Noting for now.
    }

    @Override
    public void hide() {
        //Noting for now.
    }

    @Override
    public void dispose() {
        stage.dispose();
        table.remove();
    }
}
