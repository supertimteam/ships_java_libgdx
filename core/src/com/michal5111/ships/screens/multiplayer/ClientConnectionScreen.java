package com.michal5111.ships.screens.multiplayer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.esotericsoftware.kryonet.Client;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisTextField;
import com.michal5111.ships.screens.SwitchPlayerScreen;
import com.michal5111.ships.shipsGame.ShipsGame;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by Michal on 08.11.2017.
 */

class ClientConnectionScreen implements Screen {
    private final Stage stage;
    private final Table table;
    private final ShipsGame shipsGame;

    public ClientConnectionScreen(final Client client) {
        this.shipsGame = ShipsGame.getInstance();
        final VisTextField ipAddressField = new VisTextField("");
        Label infoLabel = new VisLabel(shipsGame.getBundle().get("typeIP"));
        final Label resultLabel = new VisLabel("");
        TextButton textButton = new VisTextButton(shipsGame.getBundle().get("connect"));
        TextButton searchLanButton = new VisTextButton(shipsGame.getBundle().get("searchLan"));
        table = new Table();
        table.setFillParent(true);
        table.add(infoLabel).row();
        table.add(ipAddressField)
                .expand()
                .height(200)
                .fillX()
                .row();
        table.add(textButton)
                .expand()
                .bottom()
                .center()
                .padRight(Gdx.graphics.getWidth() / 5f)
                .padLeft(Gdx.graphics.getWidth() / 5f)
                .height(Gdx.graphics.getHeight() / 5f)
                .fillX()
                .row();
        table.add(searchLanButton)
                .expand()
                .bottom()
                .center()
                .padRight(Gdx.graphics.getWidth() / 5f)
                .padLeft(Gdx.graphics.getWidth() / 5f)
                .height(Gdx.graphics.getHeight() / 5f)
                .fillX()
                .row();
        table.add(resultLabel);
        stage = new Stage();
        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);
        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                try {
                    client.connect(50000, ipAddressField.getText(), 54555, 54777);
                    shipsGame.setScreen(new SwitchPlayerScreen(shipsGame.getBundle().format("playerTurn", (shipsGame.getTurn() + 1))));
                } catch (IOException e) {
                    resultLabel.setText(shipsGame.getBundle().get("unableToConnect"));
                    Gdx.app.log("Connection", e.getMessage(), e);
                }
            }
        });
        searchLanButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                try {
                    InetAddress address = client.discoverHost(54777, 5000);
                    client.connect(50000, address, 54555, 54777);
                    shipsGame.setScreen(new SwitchPlayerScreen(shipsGame.getBundle().format("playerTurn", (shipsGame.getTurn() + 1))));
                } catch (IOException e) {
                    resultLabel.setText(shipsGame.getBundle().get("unableToConnect"));
                    Gdx.app.log("Connection", e.getMessage(), e);
                } catch (IllegalArgumentException e) {
                    resultLabel.setText(shipsGame.getBundle().get("unableToConnect"));
                    Gdx.app.log("Connection", e.getMessage(), e);
                }
            }
        });
    }

    @Override
    public void show() {
        //Nothing for now
    }

    @Override
    public void render(float delta) {
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        //Noting for now
    }

    @Override
    public void pause() {
        //Noting for now
    }

    @Override
    public void resume() {
        //Noting for now
    }

    @Override
    public void hide() {
        //Noting for now
    }

    @Override
    public void dispose() {
        stage.dispose();
        table.remove();
    }
}
