package com.michal5111.ships.screens.multiplayer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.esotericsoftware.kryonet.Server;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.michal5111.ships.screens.SwitchPlayerScreen;
import com.michal5111.ships.shipsGame.ShipsGame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by Michal on 08.11.2017.
 */

class HostWaitForConnectionScreen implements Screen {
    private final Stage stage;
    private final Table table;
    private final TextButton textButton;
    private final ShipsGame shipsGame;
    private final Server server;

    public HostWaitForConnectionScreen(Server server) {
        this.server = server;
        this.shipsGame = ShipsGame.getInstance();
        textButton = new VisTextButton(shipsGame.getBundle().get("connectionWait"));
        Label ipLabel;
        try {
            ipLabel = new VisLabel(shipsGame.getBundle().format("yourIP", getIp()));
        } catch (IOException e) {
            ipLabel = new VisLabel(shipsGame.getBundle().format("yourIP", shipsGame.getBundle().get("noIP")));
            Gdx.app.log("IPAddress", e.getMessage(), e);
        }
        table = new Table();
        table.add(ipLabel).row();
        table.setFillParent(true);
        table.add(textButton)
                .expand()
                .bottom()
                .center()
                .padRight(Gdx.graphics.getWidth() / 5f)
                .padLeft(Gdx.graphics.getWidth() / 5f)
                .height(Gdx.graphics.getHeight() / 5f)
                .fillX();
        stage = new Stage();
        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);
    }

    private static String getIp() throws IOException {
        URL whatismyip = new URL("http://checkip.amazonaws.com");
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));
            return in.readLine();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Gdx.app.log("ERROR",e.getMessage());
                }
            }
        }
    }

    @Override
    public void show() {
        //Nothing for now
    }

    @Override
    public void render(float delta) {
        stage.draw();
        if (server.getConnections().length > 0) {
            shipsGame.setScreen(new SwitchPlayerScreen(shipsGame.getBundle().format("playerTurn", (shipsGame.getTurn() + 1))));
        }
    }

    @Override
    public void resize(int width, int height) {
        //Noting for now
    }

    @Override
    public void pause() {
        //Noting for now
    }

    @Override
    public void resume() {
        //Noting for now
    }

    @Override
    public void hide() {
        //Noting for now
    }

    @Override
    public void dispose() {
        textButton.remove();
        stage.dispose();
        table.remove();
    }
}
