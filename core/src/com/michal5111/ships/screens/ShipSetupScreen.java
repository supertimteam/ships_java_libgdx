package com.michal5111.ships.screens;

import com.badlogic.gdx.Screen;
import com.kotcrab.vis.ui.widget.VisTable;
import com.michal5111.ships.board.Board;
import com.michal5111.ships.shipsGame.ShipsGame;
import com.michal5111.ships.utility.Constants;

/**
 * Created by michal on 03.03.2018.
 */

public class ShipSetupScreen implements Screen {

    public ShipSetupScreen() {
        ShipsGame shipsGame = ShipsGame.getInstance();
        VisTable visTable = new VisTable();
        Board board = new Board(Constants.BOARD_SIZE,Constants.BOARD_SIZE);
        visTable.add(board.getBoardTable());
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
