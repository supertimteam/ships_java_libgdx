package com.michal5111.ships.shipsGame;

import com.badlogic.gdx.audio.Music;

/**
 * Created by Michal on 16.11.2017.
 */

public class SoundManager {
    private final Music bGMusic;
    private float effectsVolume;

    public SoundManager() {
        ShipsGame shipsGame = ShipsGame.getInstance();
        effectsVolume = shipsGame.getPreferences().contains("effectsVolume") ? shipsGame.getPreferences().getFloat("effectsVolume") : 1;
        bGMusic = shipsGame.getAssetManager().get("bGMusic.ogg");
        bGMusic.setLooping(true);
        bGMusic.setVolume(shipsGame.getPreferences().contains("musicVolume") ? shipsGame.getPreferences().getFloat("musicVolume") : 0.5f);
        if (!shipsGame.getPreferences().contains("musicOn") || shipsGame.getPreferences().getBoolean("musicOn")) {
            bGMusic.play();
        }
    }

    public Music getBGMusic() {
        return bGMusic;
    }

    public void dispose() {
        bGMusic.dispose();
    }

    public float getEffectsVolume() {
        return effectsVolume;
    }

    public void setEffectsVolume(float effectsVolume) {
        this.effectsVolume = effectsVolume;
    }
}
