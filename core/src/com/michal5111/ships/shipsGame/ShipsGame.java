package com.michal5111.ships.shipsGame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.I18NBundle;
import com.michal5111.ships.players.Player;
import com.michal5111.ships.screens.LoadScreen;

/**
 * Created by Michal on 08.11.2017.
 */

public class ShipsGame extends Game {
    private int turn;
    private int rounds;

    private Player[] players;
    private AssetManager assetManager;
    private SoundManager soundManager;
    private Preferences preferences;
    private I18NBundle bundle;

    private static ShipsGame instance;
    @Override
    public void create() {
        turn = 0;
        rounds = 0;
        assetManager = new AssetManager();
        instance = this;
        this.setScreen(new LoadScreen());
    }

    public void afterLoading() {
        preferences = Gdx.app.getPreferences("Preferences");
        soundManager = new SoundManager();
        bundle = assetManager.get("MyBundle");
        players = new Player[2];
    }

    public void incrementRounds() {
        rounds++;
    }

    @Override
    public void dispose() {
        assetManager.dispose();
        soundManager.dispose();
    }

    public Player[] getPlayers() {
        return players;
    }

    public int getTurn() {
        return turn;
    }

    public void switchTurn() {
        turn = turn == 0 ? 1 : 0;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public SoundManager getSoundManager() {
        return soundManager;
    }

    public Preferences getPreferences() {
        return preferences;
    }

    public int getRounds() {
        return rounds;
    }

    public I18NBundle getBundle() {
        return bundle;
    }

    public static ShipsGame getInstance() {
        return instance;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }
}
