package com.michal5111.ships.events;

import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by Michal on 10.11.2017.
 */

public abstract class SelectFieldEventListener implements EventListener {
    @Override
    public boolean handle(Event event) {
        if (!(event instanceof SelectFieldEvent)) return false;
        select(event.getStage());
        return true;
    }

    public abstract void select(Stage stage);

    public static class SelectFieldEvent extends Event {
        private final Stage stage;

        public SelectFieldEvent(Stage stage) {
            this.stage = stage;
        }

        @Override
        public Stage getStage() {
            return stage;
        }
    }
}
