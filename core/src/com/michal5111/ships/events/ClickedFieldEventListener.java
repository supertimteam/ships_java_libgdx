package com.michal5111.ships.events;

import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.michal5111.ships.utility.Point;

/**
 * Created by Michal on 15.11.2017.
 */

public abstract class ClickedFieldEventListener implements EventListener {
    @Override
    public boolean handle(Event event) {
        if (!(event instanceof ClickedFieldEvent)) return false;
        clicked(event, event.getStage());
        return true;
    }

    public abstract void clicked(Event event, Stage stage);

    public static class ClickedFieldEvent extends Event {
        private final Point clickedPoint;

        public ClickedFieldEvent(Point clickedPoint) {
            this.clickedPoint = clickedPoint;
        }

        public Point getClickedPoint() {
            return clickedPoint;
        }
    }
}
