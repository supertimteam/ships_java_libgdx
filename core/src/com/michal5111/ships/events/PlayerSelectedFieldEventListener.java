package com.michal5111.ships.events;

import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.michal5111.ships.utility.Point;

/**
 * Created by Michal on 10.11.2017.
 */

public abstract class PlayerSelectedFieldEventListener implements EventListener {
    @Override
    public boolean handle(Event event) {
        if (!(event instanceof PlayerSelectedFieldEvent)) return false;
        select((PlayerSelectedFieldEvent) event);
        return true;
    }

    public abstract void select(PlayerSelectedFieldEvent event);

    public static class PlayerSelectedFieldEvent extends Event {
        private final Point selectedField;

        public PlayerSelectedFieldEvent(Point selectedField) {
            this.selectedField = selectedField;
        }

        public Point getSelectedField() {
            return selectedField;
        }
    }
}
